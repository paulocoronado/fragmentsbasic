package com.example.fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {
    FrameLayout frameLayout;
    Fragment fragment1;
    Fragment fragment2;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Button button1;
    Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inicializar los objetos
        frameLayout = findViewById(R.id.frameLayout);
        fragment1 = new Fragmento1();
        fragment2 = new Fragmento2();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        button1 = findViewById(R.id.btFragmento1);
        button2 = findViewById(R.id.btFragmento2);


        //Add the fragment to the transaction
        fragmentTransaction.add(R.id.frameLayout, fragment1);
        //Commit the transaction
        fragmentTransaction.commit();

        //Button listener to change the fragment
        button1.setOnClickListener((v) -> {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout, fragment1);
            fragmentTransaction.commit();
        });

        button2.setOnClickListener((v) -> {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout, fragment2);
            fragmentTransaction.commit();
        });




    }
}